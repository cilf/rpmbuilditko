
#rpm build spec file

Summary: RPM builditko
Name: rpmbuilditkotest
Version: aplha
Release: 0.1
License: Copyright by Funlife
Group: System Environment/Internet
BuildRoot: %{_topdir}


%description
rpm builditko 

# Prep is used to set up the environment for building the rpm package
# Expansion of source tar balls are done in this section
%prep

# Used to compile and to build the source
# v package nepotrebujeme build veci, unit testy
%build
#make 

%install
mkdir -p /home/www/rpmbuilditkotest
cp -r %{_topdir}/* /home/www/rpmbuilditkotest

%clean
[ "${RPM_BUILD_ROOT}" != "/" ] && rm -rf ${RPM_BUILD_ROOT}

%files
/home/www/rpmbuilditkotest

%defattr(755,root,root)
%dir /home/
%dir /home/www/
%dir /home/www/rpmbuilditkotest/

%post

# Used to store any changes between versions
%changelog